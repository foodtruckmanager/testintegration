package com.example.demo.repositories;

import com.example.demo.model.Dish;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DishesRepo extends JpaRepository<Dish, Integer> {
}
